package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot maxEmptyPositionLot = parkingLots.stream()
                .max(Comparator.comparing(ParkingLot::getEmptyPositions))
                .orElse(null);

        if (maxEmptyPositionLot != null && !maxEmptyPositionLot.isFull()) {
            return maxEmptyPositionLot.park(car);
        }

        throw new FullCapacityException();
    }
}
