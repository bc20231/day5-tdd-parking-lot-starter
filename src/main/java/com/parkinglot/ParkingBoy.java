package com.parkinglot;

import java.util.List;
import java.util.Optional;

public class ParkingBoy {
    protected final List<ParkingLot> parkingLots;

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
        Optional<ParkingLot> availableLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst();

        if (availableLot.isPresent()) {
            return availableLot.get().park(car);
        }

        throw new FullCapacityException();
    }

    public Car fetch(Ticket ticket) {
        Optional<ParkingLot> fromParkingLot = parkingLots.stream()
                .filter(parkingLot -> parkingLot.isValidTicket(ticket))
                .findFirst();

        if (fromParkingLot.isPresent()) {
            return fromParkingLot.get().fetch(ticket);
        }

        throw new UnrecognizedTicketException();
    }
}
