package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {
    private final int DEFAULT_CAPACITY = 10;
    private final int capacity;
    private HashMap<Ticket, Car> parkingRecords = new HashMap<>();

    public ParkingLot() {
        this.capacity = DEFAULT_CAPACITY;
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public Ticket park(Car car) throws FullCapacityException {
        if (isFull()) {
            throw new FullCapacityException();
        } else {
            Ticket newTicket = new Ticket();
            this.parkingRecords.put(newTicket, car);
            return newTicket;
        }
    }

    public Car fetch(Ticket ticket) throws UnrecognizedTicketException {
        if (!isValidTicket(ticket))
            throw new UnrecognizedTicketException();
        return parkingRecords.remove(ticket);
    }

    public boolean isFull() {
        return this.capacity <= this.parkingRecords.size();
    }

    public boolean isValidTicket(Ticket ticket) {
        return parkingRecords.containsKey(ticket);
    }

    public int getEmptyPositions() {
        return this.capacity - this.parkingRecords.size();
    }
}
