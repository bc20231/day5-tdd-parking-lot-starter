package com.parkinglot;

public class FullCapacityException extends RuntimeException {
    public FullCapacityException() {
        super("No available position.");
    }
}
