package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperParkingBoy extends ParkingBoy {
    public SuperParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot maxAvailabilityRateLot = parkingLots.stream()
                .filter(parkingLot -> parkingLot.getCapacity() != 0)
                .max(Comparator.comparing(parkingLot -> (double) parkingLot.getEmptyPositions() / parkingLot.getCapacity()))
                .orElse(null);

        if (maxAvailabilityRateLot != null && !maxAvailabilityRateLot.isFull()) {
            return maxAvailabilityRateLot.park(car);
        }

        throw new FullCapacityException();
    }
}
