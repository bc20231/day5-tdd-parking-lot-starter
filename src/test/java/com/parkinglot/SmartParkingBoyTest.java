package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {

    @Test
    public void should_park_in_second_parking_lot_when_park_given_second_lot_has_more_spaces() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when
        Ticket ticket = smartParkingBoy.park(car);

        //then
        assertEquals(car, secondParkingLot.fetch(ticket));
    }

    @Test
    public void should_park_in_second_parking_lot_when_park_given_second_and_third_lot_have_max_spaces() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(5);
        ParkingLot thirdParkingLot = new ParkingLot(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(firstParkingLot, secondParkingLot, thirdParkingLot));
        Car car = new Car();

        //when
        Ticket ticket = smartParkingBoy.park(car);

        //then
        assertEquals(car, secondParkingLot.fetch(ticket));
    }

    @Test
    public void should_return_right_car_from_right_lot_when_fetch_given_two_lots_with_same_empty_spaces() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car firstCar = new Car();
        Ticket firstTicket = smartParkingBoy.park(firstCar);
        Car secondCar = new Car();
        Ticket secondTicket = smartParkingBoy.park(secondCar);

        //when
        Car firstParkedCar = smartParkingBoy.fetch(firstTicket);
        Car secondParkedCar = smartParkingBoy.fetch(secondTicket);

        //then
        assertEquals(firstCar, firstParkedCar);
        assertEquals(secondCar, secondParkedCar);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_wrong_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();
        smartParkingBoy.park(car);
        Ticket ticket = new Ticket();

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            smartParkingBoy.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_used_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();
        Ticket ticket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(ticket);

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            smartParkingBoy.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_full_capacity_exception_when_park_given_two_full_parking_lot_and_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(0);
        ParkingLot secondParkingLot = new ParkingLot(0);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when

        //then
        String exceptionMessage = assertThrows(FullCapacityException.class, () -> {
            smartParkingBoy.park(car);
        }).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }
}
