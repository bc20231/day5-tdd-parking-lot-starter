package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotTest {

    @Test
    public void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        Ticket ticket = parkingLot.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_with_one_car_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        //when
        Car parkedCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, parkedCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_with_cars_and_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car firstCar = new Car();
        Ticket firstTicket = parkingLot.park(firstCar);
        Car secondCar = new Car();
        Ticket secondTicket = parkingLot.park(secondCar);

        //when
        Car firstParkedCar = parkingLot.fetch(firstTicket);
        Car secondParkedCar = parkingLot.fetch(secondTicket);

        //then
        assertEquals(firstCar, firstParkedCar);
        assertEquals(secondCar, secondParkedCar);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        parkingLot.park(car);
        Ticket ticket = new Ticket();

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLot.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLot.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_full_capacity_exception_when_park_given_full_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingLot.park(car1);
        parkingLot.park(car2);

        //when

        //then
        String exceptionMessage = assertThrows(FullCapacityException.class, () -> {
            parkingLot.park(car3);
        }).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }
}