package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingBoyTest {

    @Test
    public void should_park_in_first_parking_lot_when_park_given_parking_boy_and_two_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        assertEquals(car, firstParkingLot.fetch(ticket));
    }

    @Test
    public void should_park_in_second_parking_lot_when_park_given_parking_boy_and_first_parking_lot_full() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(0);
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        assertEquals(car, secondParkingLot.fetch(ticket));
    }

    @Test
    void should_return_right_car_when_fetch_twice_given_parking_boy_with_two_parking_lots() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car firstCar = new Car();
        Ticket firstTicket = parkingBoy.park(firstCar);
        Car secondCar = new Car();
        Ticket secondTicket = parkingBoy.park(secondCar);

        //when
        Car firstParkedCar = parkingBoy.fetch(firstTicket);
        Car secondParkedCar = parkingBoy.fetch(secondTicket);

        //then
        assertEquals(firstCar, firstParkedCar);
        assertEquals(secondCar, secondParkedCar);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_parking_boy_and_two_parking_lots_and_wrong_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();
        parkingBoy.park(car);
        Ticket ticket = new Ticket();

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_parking_boy_and_two_parking_lots_and_used_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_full_capacity_exception_when_park_given_two_full_parking_lot_and_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(0);
        ParkingLot secondParkingLot = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when

        //then
        String exceptionMessage = assertThrows(FullCapacityException.class, () -> {
            parkingBoy.park(car);
        }).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }
}
