package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperParkingBoyTest {

    @Test
    public void should_park_in_second_parking_lot_when_park_given_second_lot_has_higher_available_rate() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(3);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car carParkedAtFirstLot = new Car();
        superParkingBoy.park(carParkedAtFirstLot);
        Car actual = new Car();

        //when
        Ticket ticket = superParkingBoy.park(actual);

        //then
        assertEquals(actual, secondParkingLot.fetch(ticket));
    }

    @Test
    public void should_park_in_first_parking_lot_when_park_given_two_lots_with_same_available_rate() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when
        Ticket ticket = superParkingBoy.park(car);

        //then
        assertEquals(car, firstParkingLot.fetch(ticket));
    }

    @Test
    public void should_return_right_car_from_right_lot_when_fetch_given_two_lots_with_same_available_rate() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car firstCar = new Car();
        Ticket firstTicket = superParkingBoy.park(firstCar);
        Car secondCar = new Car();
        Ticket secondTicket = superParkingBoy.park(secondCar);

        //when
        Car firstParkedCar = superParkingBoy.fetch(firstTicket);
        Car secondParkedCar = superParkingBoy.fetch(secondTicket);

        //then
        assertEquals(firstCar, firstParkedCar);
        assertEquals(secondCar, secondParkedCar);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_wrong_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();
        superParkingBoy.park(car);
        Ticket ticket = new Ticket();

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            superParkingBoy.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_unrecognized_ticket_exception_when_fetch_given_used_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();
        Ticket ticket = superParkingBoy.park(car);
        superParkingBoy.fetch(ticket);

        //when

        //then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, () -> {
            superParkingBoy.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    void should_throw_full_capacity_exception_when_park_given_two_full_parking_lot_and_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(0);
        ParkingLot secondParkingLot = new ParkingLot(0);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when

        //then
        String exceptionMessage = assertThrows(FullCapacityException.class, () -> {
            superParkingBoy.park(car);
        }).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }
}
